﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Database
{
    // configuration of entities and relationships
    public class ModelConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag>(entity =>
            {
                entity.ToTable("Tags");

                entity.HasKey(e => e.TagId);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasMany(t => t.TagToUsers)
                    .WithOne(u => u.Tag)
                    .HasForeignKey(t => t.TagId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users");

                entity.HasKey(e => e.UserId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasMany(u => u.TagToUsers)
                    .WithOne(t => t.User)
                    .HasForeignKey(t => t.UserId)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<TagToUser>(entity =>
            {
                entity.ToTable("TagToUsers");

                entity.HasKey(e => new { e.EntityId, e.UserId, e.TagId });

                entity.HasOne(t => t.User)
                    .WithMany(u => u.TagToUsers)
                    .HasForeignKey(t => t.UserId);

                entity.HasOne(t => t.Tag)
                    .WithMany(u => u.TagToUsers)
                    .HasForeignKey(t => t.TagId);
            });
        }
    }
}
