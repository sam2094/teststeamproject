﻿using Domain.Entities;

namespace DataAccess.Database
{
    public class ContextSeed
    {
        // seed data
        public static void SeedData(ApplicationDBContext context)
        {
            var users = new List<User>()
            {
                new User() { Name = "Ivan", Domain = "example.com" },
                new User() { Name = "Maria", Domain = "example.com" },
                new User() { Name = "Petr", Domain = "example.com" },
                new User() { Name = "Anna", Domain = "example2.com" },
                new User() { Name = "Sergey", Domain = "example3.com" },
            };
            context.Users.AddRange(users);

            var tags = new List<Tag>()
            {
                new Tag() { Value = "C#", Domain = "programming" },
                new Tag() { Value = "Python", Domain = "programming" },
                new Tag() { Value = "JavaScript", Domain = "programming" },
                new Tag() { Value = "SQL", Domain = "database" },
                new Tag() { Value = "Python", Domain = "ai" },
            };
            context.Tags.AddRange(tags);

            var tagToUsers = new List<TagToUser>()
            {
                new TagToUser() { UserId = users[1].UserId, TagId = tags[2].TagId },
                new TagToUser() { UserId = users[3].UserId, TagId = tags[4].TagId },
                new TagToUser() { UserId = users[3].UserId, TagId = tags[0].TagId },
                new TagToUser() { UserId = users[0].UserId, TagId = tags[2].TagId },
                new TagToUser() { UserId = users[4].UserId, TagId = tags[1].TagId },
                new TagToUser() { UserId = users[1].UserId, TagId = tags[3].TagId },
            };
            context.TagToUsers.AddRange(tagToUsers);

            context.SaveChanges();
        }
    }
}
