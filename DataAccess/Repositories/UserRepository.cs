﻿using DataAccess.Database;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDBContext _dbContext;

        public UserRepository(ApplicationDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> GetUserByIdAndDomainAsync(Guid userId, string domain)
        {
            return await _dbContext.Set<User>()
                .Where(u => u.UserId == userId && u.Domain == domain)
                .Include(u => u.TagToUsers)
                .ThenInclude(t => t.Tag)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetAllUsersByDomainAsync(string domain, int skip, int take)
        {
            return await _dbContext.Set<User>()
                .Where(u => u.Domain == domain)
                .Include(u => u.TagToUsers)
                .ThenInclude(t => t.Tag)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsersByTagAndDomainAsync(string tag, string domain)
        {
            var tagResult = await _dbContext.Set<Tag>()
                .Where(t => t.Value == tag && t.Domain == domain)
                .FirstOrDefaultAsync();

            if (tagResult == null)
            {
                return Enumerable.Empty<User>();
            }

            return await _dbContext.Set<User>()
                .Where(u => u.TagToUsers.Any(t => t.TagId == tagResult.TagId))
                .Include(u => u.TagToUsers)
                .ThenInclude(t => t.Tag)
                .ToListAsync();
        }
    }
}
