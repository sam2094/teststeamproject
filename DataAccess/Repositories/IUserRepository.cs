﻿using Domain.Entities;

namespace DataAccess.Repositories
{
    public interface IUserRepository
    {
        public Task<User> GetUserByIdAndDomainAsync(Guid userId, string domain);
        public Task<IEnumerable<User>> GetAllUsersByDomainAsync(string domain, int skip, int take);
        public Task<IEnumerable<User>> GetUsersByTagAndDomainAsync(string tag, string domain);
    }
}
