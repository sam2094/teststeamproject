﻿namespace Application
{
    public static class StreamPackageReader
    {
        public static IEnumerable<byte[]> ReadPackages(Stream inputStream, byte delimiter)
        {
            if (inputStream == null || (!inputStream.CanRead))
            {
                throw new ArgumentException("Input stream cannot be null and must be readable");
            }

            using (MemoryStream messageStream = new MemoryStream())
            {
                int currentByte;
                while ((currentByte = inputStream.ReadByte()) != -1)
                {
                    if (currentByte == delimiter)
                    {
                        // check if there is data after the last delimiter
                        if (messageStream.Length > 0)
                        {
                            // return separated package
                            yield return messageStream.ToArray();
                            messageStream.SetLength(0);
                        }
                    }
                    else
                    {
                        messageStream.WriteByte((byte)currentByte);
                    }
                }

                if (messageStream.Length > 0)
                {
                    yield return messageStream.ToArray();
                }
            }
        }
    }
}
