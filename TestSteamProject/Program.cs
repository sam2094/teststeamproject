﻿using Application;
using DataAccess.Database;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace SteamReader
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .Build();

            var services = new ServiceCollection();
            ConfigureServices(services, configuration);
            var serviceProvider = services.BuildServiceProvider();
            ConfigureAndSeedDatabase(serviceProvider);

            await GetUsersAndTags(serviceProvider);
            GetPackages(configuration);
        }

        public static void GetPackages(IConfiguration configuration)
        {
            byte delimiterByte;

            try
            {
                string delimiter = configuration["Delimiter"];
                var bytes = Encoding.UTF8.GetBytes(delimiter);

                // make sure that the delimiter is one-byte
                if (bytes.Length != 1)
                {
                    throw new InvalidDataException();
                }

                delimiterByte = bytes[0];

                string input = "The cat, with its fluffy tail, jumped onto the table, knocked over a vase, and then ran away,,";
                byte[] data = Encoding.UTF8.GetBytes(input);

                using (MemoryStream stream = new MemoryStream(data))
                {
                    IEnumerable<byte[]> packages = StreamPackageReader.ReadPackages(stream, delimiterByte);

                    foreach (byte[] package in packages)
                    {
                        Console.WriteLine($"Package: {System.Text.Encoding.UTF8.GetString(package)}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured: \n {ex.Message}");
                return;
            }
        }

        public static async Task GetUsersAndTags(ServiceProvider serviceProvider)
        {
            try
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();

                    // did not use reading from the console (ReadLine) due to the complexity of entering the Guid
                    var user = await userRepository.GetUserByIdAndDomainAsync(new Guid("5048656C-00F4-4AF0-E5C0-08DC4A856811"), "example.com");
                    var users = await userRepository.GetAllUsersByDomainAsync("example.com", 1, 5);
                    var usersByTag = await userRepository.GetUsersByTagAndDomainAsync("Python", "example.com");

                    Console.WriteLine($"User by id and domain: {user} \n");
                    Console.WriteLine($"All users by domain: {string.Join(Environment.NewLine, users)} \n");
                    Console.WriteLine($"All users by tag and domain: {string.Join(Environment.NewLine, usersByTag)} \n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }

        // creating DB and running seed method if necessary data is not exists
        public static void ConfigureAndSeedDatabase(ServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDBContext>();
                context.Database.EnsureCreated();

                if (!context.Users.Any())
                {
                    ContextSeed.SeedData(context);
                    context.SaveChanges();
                }
            }
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IConfiguration>(configuration);
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddDbContext<ApplicationDBContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }
    }
}
