﻿namespace Domain.Entities
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Name { get; set; } = default!;
        public string Domain { get; set; } = default!;
        public List<TagToUser>? TagToUsers { get; set; }

        public override string ToString()
        {
            return $"UserId: {UserId}, Name: {Name}, Domain: {Domain}, TagToUsers: {(TagToUsers != null ? TagToUsers.ToString() : " null")}";
        }
    }
}