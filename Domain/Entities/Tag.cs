﻿namespace Domain.Entities
{
    public class Tag
    {
        public Guid TagId { get; set; }
        public string Value { get; set; } = default!;
        public string Domain { get; set; } = default!;
        public List<TagToUser>? TagToUsers { get; set; }

        public override string ToString()
        {
            return $"TagId: {TagId}, Value: {Value}, Domain: {Domain}, TagToUsers: {(TagToUsers != null ? TagToUsers.ToString() : " null")}";
        }
    }
}
