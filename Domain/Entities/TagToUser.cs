﻿namespace Domain.Entities
{
    public class TagToUser
    {
        public Guid EntityId { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
        public Guid TagId { get; set; }
        public Tag? Tag { get; set; }

        public override string ToString()
        {
            return $"EntityId: {EntityId}, UserId: {UserId}, User: {(User != null ? User.ToString() : " null")}, TagId: {TagId}, Tag: {(Tag != null ? Tag.ToString() : " null")}";
        }
    }
}

